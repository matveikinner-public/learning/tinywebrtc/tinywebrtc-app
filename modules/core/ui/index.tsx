import "reflect-metadata";
import { createRoot } from "react-dom/client";
import CoreProvider from "./app/CoreProvider";

const container = document.getElementById("root");

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(container!);

root.render(<CoreProvider />);
